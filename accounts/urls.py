from django.urls import path
from accounts.views import accounts, admins, managers, customers

app_name = 'accounts'

urlpatterns = [
    path('signup/', accounts.SignUpView.as_view(), name='signup'),
    path('signup/admin/', admins.AdminSignUpView.as_view(), name='admin_signup'),
    path('signup/manager/', managers.ManagerSignUpView.as_view(), name='manager_signup'),
    path('signup/customer/', customers.CustomerSignUpView.as_view(), name='customer_signup'),
]