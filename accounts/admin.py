from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import AdminSignUpForm
from .models import User


class CustomUserAdmin(UserAdmin):
    add_form = AdminSignUpForm
    model = User
    list_display = ['email', 'username','is_admin','is_manager','is_customer']


admin.site.register(User, CustomUserAdmin)