from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    pass
    is_admin = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)
    is_customer = models.BooleanField(default=False)
    email = models.EmailField()
    address = models.TextField(max_length=200)

    def __str__(self):
        return self.username


class GuestEmail(models.Model):
    email = models.EmailField()
    active = models.BooleanField(default=True)
    update = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email




