from django.shortcuts import render

# Create your views here.


def home_page(request):
    context = {
        "title":"Home Page",
    }
    if request.user.is_authenticated :
        context["premium_content"] = "yeah"
    return render(request, "home/home.html", context)


def contact_page(request):
    context = {
        "title": "Contact Page",
    }
    return render(request, "home/contact.html", context)
