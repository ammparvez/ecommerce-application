from django.urls import path
from .views import home_page, contact_page

app_name = 'home'

urlpatterns = [
    path('', home_page, name='home'),
    path('contact', contact_page, name='contact'),
]