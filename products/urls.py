from django.urls import path
from .views import ProductListView, ProductDetailSlugView

app_name = 'products'

urlpatterns = [
    path('list', ProductListView.as_view(), name='list'),
    # path('details/<int:pk>/', ProductDetailView.as_view(), name='details'),
    path('details/<slug:slug>/',ProductDetailSlugView.as_view(),name='details')

]